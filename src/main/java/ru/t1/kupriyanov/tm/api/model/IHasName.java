package ru.t1.kupriyanov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
